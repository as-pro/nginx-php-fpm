ARG NGINX_VER=1.15.1
ARG PHP_IMAGE=registry.gitlab.com/as-pro/php-fpm:7.2.7

FROM nginx:${NGINX_VER}-alpine as nginx

FROM ${PHP_IMAGE}

COPY --from=nginx /etc/nginx /etc/nginx
COPY --from=nginx /usr/sbin/nginx /usr/sbin/nginx
COPY --from=nginx /usr/share/nginx /usr/share/nginx
COPY --from=nginx /usr/lib/nginx /usr/lib/nginx
COPY --from=nginx /var/log/nginx /var/log/nginx
COPY --from=nginx /var/cache/nginx /var/cache/nginx

RUN ln -s /usr/lib/nginx/modules /etc/nginx/modules \

	# Bring in gettext so we can get `envsubst`, then throw
	# the rest away. To do this, we need to install `gettext`
	# then move `envsubst` out of the way so `gettext` can
	# be deleted completely, then move `envsubst` back.
	&& apk add --no-cache --virtual .gettext gettext \
	&& mv /usr/bin/envsubst /tmp/ \
	\
	&& runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' /usr/sbin/nginx /usr/lib/nginx/modules/*.so /tmp/envsubst \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)" \
	&& apk add --no-cache --virtual .nginx-rundeps $runDeps \
	&& apk del .gettext \
	&& mv /tmp/envsubst /usr/local/bin/ \
	# forward request and error logs to docker log collector
	&& ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log \

	&& apk add --no-cache supervisor \
    && rm -rf /var/cache/apk/*

ADD rootfs /

RUN find /usr/local/bin -type f -exec chmod +x {} \; \
 && mkdir -p /app

WORKDIR /app
EXPOSE 80 9000
ENTRYPOINT ["docker-entrypoint"]
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]