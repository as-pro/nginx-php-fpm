#!/bin/sh

on_or_off () {
    local RESULT="on"
    local VAR=$0
    if [[ -z "$VAR" || "$VAR" == "0" || "$VAR" == "false" || "$VAR" == "off" || "$VAR" == "no" ]]; then
        RESULT="off"
    fi
    echo ${RESULT}
}


SERVER_NAME=${SERVER_NAME:="_"}
APP_DIR=${APP_DIR:="/app"}
APP_PUBLIC_DIR=${APP_PUBLIC_DIR:="${APP_DIR}/public"}
SERVER_TOKENS=`on_or_off "$SERVER_TOKENS"`
REAL_IP_HEADER=`on_or_off "$REAL_IP_HEADER"`
REAL_IP_FROM=${REAL_IP_FROM:="172.16.0.0/12"}

### dirs
mkdir -p $APP_DIR
mkdir -p $APP_PUBLIC_DIR
chown -Rf app:app $APP_DIR
chown -Rf app:app $APP_PUBLIC_DIR

NGINX_APP_CONF="/etc/nginx/app/server.conf"
sed -i \
    -e "s~server_tokens\s\+[^\n]\+~server_tokens ${SERVER_TOKENS};~" \
    -e "s~root\s\+[^\n]\+~root ${APP_PUBLIC_DIR};~" \
    -e "s~server_name\s\+[^\n]\+~server_name ${SERVER_NAME};~" \
    ${NGINX_APP_CONF}

if [[ "$REAL_IP_HEADER" == "on" ]] ; then

cat >/etc/nginx/app/conf.d/0-real-ip.conf <<EOL
real_ip_header X-Forwarded-For;
set_real_ip_from ${REAL_IP_FROM};
EOL

fi