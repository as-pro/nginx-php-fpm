server {
	listen   80; ## listen for ipv4; this line is default and implied
	listen   [::]:80 default ipv6only=on; ## listen for ipv6

	root /app/public;
	index index.php index.html index.htm;

	# Make site accessible from http://localhost/
	server_name _;

	sendfile off;

    client_max_body_size 50m;

	# Add stdout logging
	error_log /dev/stdout info;
	access_log /dev/stdout;

	include /etc/nginx/app/conf.d/*.conf;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass 127.0.0.1:9000;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $request_filename;
        fastcgi_read_timeout 1000;
    }

    location ~* \.(css|js|gif|ico|png|jpg|svg|json|xml)$ {
        expires 5d;
        add_header Cache-Control private;
        try_files $uri $uri/ /index.php?$query_string;
    }
}